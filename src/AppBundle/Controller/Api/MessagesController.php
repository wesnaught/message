<?php

namespace AppBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Messages;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class MessagesController
 * @package AppBundle\Controller\Api
 * @Route("/api")
 */
class MessagesController extends Controller
{
    /**
     * @Route("/get")
     * @Method("GET")
     * @return JsonResponse
     */
    public function getMessagesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $records = $em->getRepository(Messages::class)->findAll();
        $response = [];
        foreach($records as $record){
            $record = $record->toArray();
            $response[] = $record;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/put")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function putMessageAction(Request $request)
    {
        $entity = new Messages();
        $entity->setMessage($request->request->get('message'));
        $entity->setUser($request->request->get('user'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return new JsonResponse();
    }
}