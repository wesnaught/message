<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MessagesRepository extends EntityRepository
{
 public function getAll()
 {
     return $this->getEntityManager()
         ->createQuery(
             'SELECT * FROM AppBundle:Messages'
         )
         ->getResult();
 }
}